'use strict';

var expect = require('chai').expect;
var fs = require('fs-extra');
var path = require('path');
var del = require('del');
var mockSpawn = require('mock-spawn');

var GitClient = require('../../lib/gitClient');

describe('GitClient', function() {
  var workDir;

  before(function() {
    // Child process の spawsn (git コマンド実行)をモック
    // デフォルトではコマンドはすべて正常終了する
    var mySpawn = mockSpawn();
    require('child_process').spawn = mySpawn;
  });

  beforeEach(function() {
    // Create work directory.
    workDir = path.join(__dirname, '../../.tmp', new Date().getTime().toString() + '-work');
    fs.mkdirsSync(workDir);

    // update files.
    var data = new Date().getTime();
    fs.appendFile(path.join(workDir, 'README.md'), data, function (err) {
      if(err) {
        throw err;
      }
    });
  });

  afterEach(function() {
    del.sync(workDir);
  });

  it('Git に push できること', function() {
    var repository = 'https://user@bitbucket.org/user/hoge.git';

    var gitClient = new GitClient(repository);
    return gitClient.push(workDir);
  });
});

