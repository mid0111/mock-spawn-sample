'use strict';

var request = require('supertest');

var app = require('../app');
var mockSpawn = require('mock-spawn');
var expect = require('chai').expect;
var childProcess = require('child_process');
var process = require('process');
var spawn = mockSpawn();

childProcess.spawn = spawn;

describe('Git エンドポイント', function() {

  it('Git にプッシュできること', function(done) {
    // Child process の spawn (git コマンド実行)をモック
    spawn.sequence.add(spawn.simple(1));

    request(app)
      .get('/git')
      .expect(200)
      .end(function(err, res){
        if (err) throw err;
        console.log(res.body);
        expect(res.body.repository).to.be.a('string');
        done();
      });
  });

  it('Git にプッシュする時にエラーが発生した場合500 エラーとなること', function(done) {
    // Child process の spawn (git コマンド実行)をモック
    var cbcalled = false;
    spawn.sequence.add(function(cb){
      this.emit('error', new Error('spawn ENOENT'));
      process.nextTick(function(){
        cb(-1);
        cbcalled = true;
      });
    });

    request(app)
      .get('/git')
      .expect(500)
      .end(function(err, res){
        if (err) {
          throw err;
        }
        expect(res.body.message).to.be.a('string');
        done();
      });
  });
});
