'use strict';

var fs = require('fs-extra');
var path = require('path');
var del = require('del');

var GitClient = function(repository){
  this.repository = repository;
  this.gitDir = path.join(__dirname, '../.tmp', new Date().getTime().toString() + '-git');
};

GitClient.prototype.push = function(workDir) {

  fs.mkdirsSync(this.gitDir);

  return this._push(workDir)
    .then(() => {
      this._deleteWorkDir();
    })
    .catch((err) => {
      this._deleteWorkDir();
      throw err;
    });
};

GitClient.prototype._push = function(workDir) {

  var Git = require('simple-git')(this.gitDir);
  return new Promise((resolve, reject) => {
    return Git
      .init(null, (err) => {
        if(err) {
          console.log('git init failed. reason: ' + err);
          reject(new Error(err));
        }
      })
      .then(() => {
        console.log('git init complete.');

      })
      .addRemote('origin', this.repository, (err) => {
        if(err) {
          console.log('add remote fail. reason: ' + err);
          reject(new Error(err));
        }

      })
      .then(() => {
        console.log('add remote complete.');
      })
      .pull('origin', 'master', (err) => {
        if(err) {
          console.log('git pull failed. reason: ' + err);
          reject(new Error(err));
        }

      })
      .then(() => {
        console.log('pull complete.');
      })
      .then(() => {
        // 既存のコミットされているファイルを削除
        del.sync(path.join(this.gitDir + './*'));
        // work ディレクトリ内のファイルを Git 用の作業ディレクトリにコピー
        fs.copySync(workDir, this.gitDir);

      })
      .add('./*', (err) => {
        if(err) {
          console.log('git add failed. reason: ' + err);
          reject(new Error(err));
        }

      })
      .then(() => {
        console.log('add complete.');
      })
      .commit("commit!")
      .then(() => {
        console.log('commit complete.');
      })
      .push('origin', 'master', (err) => {
        if(err) {
          console.log('push fail. reason: ' + err);
          reject(new Error(err));
        }

      })
      .then(() => {
        console.log('push complete.');
        resolve();
      });
    });
};

GitClient.prototype._deleteWorkDir = function() {
  console.log('rmdir: ' + this.gitDir);
  del.sync(this.gitDir);
};

module.exports = GitClient;
