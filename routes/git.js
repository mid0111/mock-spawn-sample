var express = require('express');
var router = express.Router();

var fs = require('fs-extra');
var path = require('path');
var del = require('del');

var GitClient = require('../lib/gitClient');

router.get('/', function(req, res, next) {

  // Create work directory.
  var workDir = path.join(__dirname, '../.tmp', new Date().getTime().toString() + '-work');
  fs.mkdirsSync(workDir);

  // update files.
  var data = new Date().getTime();
  fs.appendFile(path.join(workDir, 'README.md'), data, function (err) {
    if(err) {
      throw err;
    }
  });

  var repository = 'https://user@bitbucket.org/user/hoge.git';

  var gitClient = new GitClient(repository);
  gitClient.push(workDir).then(function() {
    console.log('Success to push.');

    del.sync(workDir);

    res.json({
      repository: repository
    });

  }).catch(function(err) {
    console.log('Faild to push.', err);

    del.sync(workDir);

    res.status(500);
    res.json({
      message: err.message,
      err: err
    });
  });
});

module.exports = router;
